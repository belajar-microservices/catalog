/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.belajar.marketplace.catalog.controller;

import com.belajar.marketplace.catalog.dao.ProductDao;
import com.belajar.marketplace.catalog.entity.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author opaw
 */
@RestController @RequestMapping("/api/product")
public class ProductController {
    @Autowired 
    private ProductDao productDao;
    
    @GetMapping("/")
    public Page<Product> findProducts(Pageable page){
        return productDao.findAll(page);
    }
    
    @GetMapping("/{id}")
    public Product findById(@PathVariable("id") Product p){
        return p;
    }
}
