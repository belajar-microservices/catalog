/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.belajar.marketplace.catalog.dao;

import com.belajar.marketplace.catalog.entity.Product;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 *
 * @author opaw
 */
public interface ProductDao extends PagingAndSortingRepository<Product, String>{
    
}
